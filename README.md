# Gitlab

Gitlab pipeline templates to include in other projects. Templates will
fullfil common tasks.

### Overview

| Template | Description                                           | File                     |
|:---------|:------------------------------------------------------|:-------------------------|
| Rules    | A set of rules to control if a job should run or not  | [rules.yml](./rules.yml) |
| Tags     | Default rules to target runners and environments      | [tags.yml](./tags.yml)   |

### Usage

As global templates do not include pipeline jobs, you just need to
include them. Nothing else...

```
...
include:
  - project: 'leofy.de/ci-cd'
    ref: master
    file: 'tags.yml'
  - project: 'leofy.de/ci-cd'
    ref: master
    file: 'rules.yml'
...
```
